#!/bin/bash
set -e

snap install spt spotifyd
mkdir -p /var/tmp/spotifyd-cache
mkdir -p $HOME/.config/spotifyd
cp spotifyd.conf $HOME/.config/spotifyd/spotifyd.conf
echo "Check Config (change device name) and then run 'snap restart spotifyd'"
