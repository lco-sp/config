#!/bin/bash
set -e

sudo apt update
sudo apt upgrade
sudo apt full-upgrade
sudo apt install --install-suggests coreutils build-essential 
sudo apt install htop \
	bpytop \
	aptitude \
	neovim \
	emacs \
	zsh \
	git \
	curl \
	wget \
	tig \
	tmux \
	info \
	mupdf \
	lynx \
	nnn \
	rsync \
	screen \
	netcat \
	hexedit \
	perl \
	irssi \
	dc \
	sc \
	newsboat \
	python3 python3-pip \
	nmon \
	snapd \
	flatpak \
	ca-certificates \
	gnupg \
	backup-manager \
	ed \
	doc-rfc \
	games-finest \
	games-typing \
	games-console \
	colobot \
	robocode \
	moon-lander \


#jupyter?
#basis-sprachen? Oder Per Nix? JVM? LLVM? Language-Server?

mkdir -p $HOME/Code

sudo install -m 0755 -d /etc/apt/keyrings
curl -fsSL https://download.docker.com/linux/debian/gpg | sudo gpg --dearmor -o /etc/apt/keyrings/docker.gpg
sudo chmod a+r /etc/apt/keyrings/docker.gpg

echo \
  "deb [arch="$(dpkg --print-architecture)" signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/debian \
  "$(. /etc/os-release && echo "$VERSION_CODENAME")" stable" | \
  sudo tee /etc/apt/sources.list.d/docker.list > /dev/null

sudo apt update

sudo apt install docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin

systemctl enable --now snapd
sudo snap install core

flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo

pip install haxor-news



