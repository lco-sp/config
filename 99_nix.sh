#!/bin/sh
set -e

sh <(curl -L https://nixos.org/nix/install) --daemon
