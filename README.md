## Configs
Various configuration files, minor scripts and miscelaneous stuff.

Note-to-self:
Add
'Defaults insults'
to /etc/sudoers file, and then sudo will output insults when typing the wrong password! No need to recompile!

TODO:

Reproduzierbare Setups, Nix Basis
* ~~Kernsetup (Editor + Curl + Wget + Mail + CLI Tools für Files und Network)~~
* Desktopsetup (Gnome, I3, Openbox)
* ~~Consolen-Setup (Zsh + OMZ + Extensions)~~
* Devel (Virtualisierung + Language-Server + ~~Code Folder~~)
